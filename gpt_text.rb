require "ruby/openai"

print "請說話 say_name: "
say_name = gets

if say_name != "" 
	client = OpenAI::Client.new(
      access_token: "..."
	)  
	response = client.chat( 
	    parameters: {
            model: "gpt-4", 
            messages: [{ role: "user", content: say_name}], 
        	  temperature: 0.7,
	    }) 
	puts get_open_ai_say = response.dig("choices", 0, "message", "content")
end
