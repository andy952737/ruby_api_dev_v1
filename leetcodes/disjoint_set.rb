class UnionFind
  def initialize(size)
    @root = Array.new(size) { |i| i }
  end

  def find(x)
    if x == @root[x]
      return x
    end
    @root[x] = find(@root[x])
    return @root[x]
  end

  def union(x, y)
    rootX = find(x)
    rootY = find(y)
    if rootX != rootY
      @root[rootY] = rootX
    end
  end

  def connected?(x, y)
    find(x) == find(y)
  end
end

# Test Case
uf = UnionFind.new(10)
# 1-2-5-6-7 3-8-9 4
uf.union(1, 2)
uf.union(2, 5)
uf.union(5, 6)
uf.union(6, 7)
uf.union(3, 8)
uf.union(8, 9)
puts uf.connected?(1, 5)  # true
puts uf.connected?(5, 7)  # true
puts uf.connected?(4, 9)  # false
# 1-2-5-6-7 3-8-9-4
uf.union(9, 4)
puts uf.connected?(4, 9)  # true
