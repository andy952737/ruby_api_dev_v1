# ruby api file v1 

require 'uri'
require 'net/http'
require 'cgi'
require 'json'

@api_key = 'your api key'
@secret_key = 'your secret key'

url = URI('your api url')
http = Net::HTTP.new(url.host, url.port)
http.use_ssl = true
request = Net::HTTP::Post.new(url)
request['Content-Type'] = 'application/x-www-form-urlencoded'

    request.body = [
        "grant_type=client_credentials",
        "client_id=#{@api_key}",
        "client_secret=#{@secret_key}"
    ].join('&')

  response = http.request(request)
  
  response_data = JSON.parse(response.body)
  puts response_data['your get value']

