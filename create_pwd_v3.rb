def generate_password(length = 20, min_length = 10, max_length = 40)
  chars = ("a".."z").to_a + ("A".."Z").to_a + ("0".."9").to_a

  while true
    password = Array.new(rand(min_length..max_length)) { chars.sample }.join
    break if strong_password?(password)
  end

  password
end

def strong_password?(password)
  # 檢查密碼是否同時包含大小寫字母和數字
  password =~ /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).{8,}$/ 
end

password = generate_password
puts password
