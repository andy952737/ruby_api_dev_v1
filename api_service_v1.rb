module Shipping
  class FedexService 
   
    require 'uri' 
    require 'net/http'
    require 'cgi'
    require 'json'

    # api urls:
    # 正式環境：apis.fedex.com
    # 測試環境：apis-sandbox.fedex.com
 
    def initialize(shop_id, tracking_number, env)  
      @shop_id = 'your id' 
      @api_key = Setting.find_by(var: "fedex_api_key", shop_id: shop_id).value
      @secret_key = Setting.find_by(var: "fedex_secret_key", shop_id: shop_id).value

      @token = call_token  
      @tracking_number = tracking_number

      # "test" or "production"
      @env = env
    end 
 
    # Shipping::FedexService.new("id","number", "test").call_trackingnumbers
    def call_trackingnumbers
      if @env == "test"
        url = URI("url")
      else
        url = URI("url")
      end

      http = Net::HTTP.new(url.host, url.port) 
      http.use_ssl = true  

      request = Net::HTTP::Post.new(url)
      request["Content-Type"] = "application/json"
      request["Authorization"] = "Bearer #{@token}"

      request.body = JSON.generate({
        "includeDetailedScans": true,
        "trackingInfo": [
          {
            "trackingNumberInfo": {
              "trackingNumber": @tracking_number
            }
          }
        ]
      })

      response = http.request(request)
      r = JSON.parse(response.read_body)
      
      scan_events = r.dig('output', 'completeTrackResults', 0, 'trackResults', 0, 'scanEvents') 

      # 這要存下來的值
      # transactionId  
      transactionId = r["transactionId"]

      # 取得貨物運送狀態
      derivedStatus = r.dig('output', 'completeTrackResults', 0, 'trackResults', 0, 'scanEvents',0,'derivedStatus') 

      if response.code == "200"
        get_response = {
          response_code: "200",
          transactionId: transactionId,
          derivedStatus: derivedStatus,
          scan_events: scan_events
        }
        #return get_response
      else
        raise "Error: #{response.code}"
      end

    end  

    private  

    # Shipping::FedexService.new("shop_id").call_token
    def call_token 
      if @env == "test"
        url = URI('url')
      else
        url = URI('url')
      end

      response = http_post_body(url,
      {
        api_key: @api_key,
        secret_key: @secret_key
      })

      begin
        response_data = JSON.parse(response.body)
        access_token = response_data['access_token']
      rescue JSON::ParserError => e
        raise "Failed to parse JSON response: #{e.message}"
      rescue => e
        raise "An error occurred: #{e.message}"
      end 
    end

    def http_post_body(url, data={})
      http = Net::HTTP.new(url.host, url.port)
      http.use_ssl = true  
      request = Net::HTTP::Post.new(url)   
      request['Content-Type'] = 'application/x-www-form-urlencoded'  

      request.body = [
        "grant_type=client_credentials",
        "client_id=#{data[:api_key]}",
        "client_secret=#{data[:secret_key]}"
      ].join('&') 

      http.request(request)
    end

  end
end
