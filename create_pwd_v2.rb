def generate_password(length = 12, min_length = 8, max_length = 32)
  chars = ("a".."z").to_a + ("A".."Z").to_a + ("0".."9").to_a + ["!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "-", "+", "=", "_", "{", "}", "[", "]", "|", "'", ".", "?", "`", "~"]

  while true
    password = Array.new(rand(min_length..max_length)) { chars.sample }.join
    break if strong_password?(password)
  end

  password
end

def strong_password?(password)
  password =~ /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[\W]).{8,}$/
end

password = generate_password
puts password
