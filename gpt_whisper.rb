require 'http'
require 'json'
require 'openai'

open_ai_api_key = 'Your of open ai api key'
file_url = 'test.m4a'
audio_file = File.open(file_url, "rb")

response = HTTP.auth("Bearer #{open_ai_api_key}")
               .post("https://api.openai.com/v1/audio/transcriptions", form: {
                 model: "whisper-1",
                 file: HTTP::FormData::File.new(audio_file)
               })

get_audio_text = JSON.parse(response.body.to_s)["text"]

client = OpenAI::Client.new(access_token: open_ai_api_key)  
response = client.chat( 
      parameters: {
            model: "gpt-4", 
            messages: [{ role: "user", content: get_audio_text}], 
            temperature: 0.7,
      }) 
puts get_open_ai_say = response.dig("choices", 0, "message", "content")