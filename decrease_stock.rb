class Product
  attr_accessor :id, :stock

  def initialize(id, stock)
    @id = id
    @stock = stock
  end

  # 模擬 lock 方法（實際上在 ActiveRecord 中是透過資料庫鎖定的）
  def self.lock
    self
  end

  # 模擬 find 方法
  def self.find(product_id)
    # 假設我們只有一個產品，stock 為 10
    Product.new(product_id, 10)
  end

  # 模擬 save! 方法
  def save!
    puts "Product #{@id} stock updated to: #{@stock}"
  end
end

def decrease_stock(product_id, quantity)
  begin
    # 模擬 transaction 行為
    puts "Starting transaction..."
    
    # 鎖定產品以防止其他交易同時修改
    product = Product.lock.find(product_id)
    
    if product.stock >= quantity
      product.stock -= quantity
      product.save!
    else
      raise "Insufficient stock"
    end
  rescue => e
    puts "Failed to decrease stock: #{e.message}"
  end
end

# 測試函式
decrease_stock(1, 5) # 減少 5 庫存
decrease_stock(1, 7) # 減少 7 庫存，模擬庫存不足

